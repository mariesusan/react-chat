import './App.css';
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore'
import 'firebase/compat/auth'

import {useAuthState} from 'react-firebase-hooks/auth'
import {useCollectionData} from 'react-firebase-hooks/firestore'
import { useState, useRef } from 'react';

firebase.initializeApp({
  // config
  apiKey: "AIzaSyDYdqHljCYrfBq1krTG35l7KVkJOTrqo3s",
  authDomain: "superchat-71b91.firebaseapp.com",
  projectId: "superchat-71b91",
  storageBucket: "superchat-71b91.appspot.com",
  messagingSenderId: "1036014419293",
  appId: "1:1036014419293:web:2a436391dff204582b3cb4",
  measurementId: "G-2MJB93LD83"
})

const auth = firebase.auth()
const firestore = firebase.firestore()

function App() {

  const [user] = useAuthState(auth)

  return (
    <div className="App">
      <header>
        <h1>⚛️🔥💬</h1>
        <SignOut />
      </header>
      <section>
        {user ? <Chatroom/> : <SignIn/>}
      </section>
    </div>
  );
}

function Chatroom () {
  const dummy = useRef()
  const messagesRef = firestore.collection('messages')
  const query = messagesRef.orderBy('createdAt').limit(25)

  const [messages] = useCollectionData(query, {idField: 'id'})

  const [formValue, setFormValue] = useState('')

  const sendMessage = async(e) => {
    e.preventDefault()
    const {uid, photoURL} = auth.currentUser
    await messagesRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid,
      photoURL
    })
    setFormValue('')
    dummy.current.scrollIntoView({ behavior: 'smooth' });
  }

  return (
    <>
      <main>
        {messages && messages.map(msg => <ChatMessage key = {msg.id} message = {msg}/>)}
        <span ref = {dummy}></span>
      </main>
      <form onSubmit = {sendMessage}>
        <input value = {formValue} onChange = {(e) => setFormValue(e.target.value)} placeholder = 'say something nice :)'/>
        <button type = 'submit' disabled = {!formValue}>🕊️</button>
      </form>
    </>
  )
}

function ChatMessage (props) {
  const {text, uid, photoURL} = props.message
  const messageClass = uid === auth.currentUser.uid ? 'sent' : 'received'
  return (
    <div className = {`message ${messageClass}`}>
      <img src = {photoURL || 'https://api.adorable.io/avatars/23/abott@adorable.png'} alt = ''/>
      <p>{text}</p>
    </div>
  )
}

function SignIn () {
  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider()
    auth.signInWithPopup(provider)
  }

  return (
    <button className='sign-in' onClick = {signInWithGoogle}>Sign in with Google</button>
  )
}

function SignOut () {
  return (
    auth.currentUser && (
      <button className='sign-out' onClick = {() => auth.SignOut()}>Sign Out</button>
    )
  )
}

export default App;
